#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"

void tester() 
{
	/*int get_next_book_id() 
{
	FILE *fp;
	int max = 0;
	int size = sizeof(book_t);
	book_t u;
	// open the file
	fp = fopen(BOOK_DB, "rb");
	if(fp == NULL)
		return max + 1;
	// change file pos to the last record
	fseek(fp, -size, SEEK_END);
	// read the record from the file
	if(fread(&u, size, 1, fp) > 0)
		// if read is successful, get max (its) id
		max = u.id;
	// close the file
	fclose(fp);
	printf("%d",u.id);
	// return max + 1
	
}*/

}

void sign_in() 
{
	char email[30], password[10];
	user_t u;
	int invalid_user = 0;

	printf("email: ");
	scanf("%s", email);
	printf("password: ");
	scanf("%s", password);

	if(user_find_by_email(&u, email) == 1) 
	{
	
		if(strcmp(password, u.password) == 0) 
		{
			
			if(strcmp(email, EMAIL_OWNER) == 0)
				strcpy(u.role, ROLE_OWNER);

		
			if(strcmp(u.role, ROLE_OWNER) == 0)
				owner_area(&u);
			else if(strcmp(u.role, ROLE_LIBRARIAN) == 0)
				librarian_area(&u);
			else if(strcmp(u.role, ROLE_MEMBER) == 0)
				member_area(&u);
			else
				invalid_user = 1;
		}
		else
			invalid_user = 1;
	}
	else
		invalid_user = 1;

	if(invalid_user)
		printf("Invalid email, password or role.\n");
}

void sign_up() 
{
	
	user_t u;
	user_accept(&u);
	user_add(&u);
}

int main() 
{
	
	int choice;
	do 
	{
		printf("\n\n0. Exit\n1. Sign In\n2. Sign Up\nEnter choice: ");
		scanf("%d", &choice);
		switch (choice)
		{
		case 1: // Sign In
			sign_in();
			break;
		case 2:	// Sign Up
			sign_up();	
			break;
		}
	}while(choice != 0);
	
	return 0;
	
}
