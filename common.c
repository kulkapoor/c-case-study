#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "library.h"

void user_accept(user_t *u) 
{
	u->id = get_next_user_id();
	printf("name: ");
	scanf("%s", u->name);
	printf("email: ");
	scanf("%s", u->email);
	printf("phone: ");
	scanf("%s", u->phone);
	printf("password: ");
	scanf("%s", u->password);
	strcpy(u->role, ROLE_MEMBER);
}

void user_add(user_t *u) 
{
	FILE *fp;
	fp = fopen(USER_DB, "ab");
	if(fp == NULL) {
		perror("failed to open users file");
		return;
	}
	fwrite(u, sizeof(user_t), 1, fp);
	printf("user added into file.\n");
	fclose(fp);
}
/*
void edit_profile(user_t *u){
     char p1[35];
      char p2[35];
      long size;
    
    FILE *fp;
    fp= fopen(USER_DB, "rb+");
    if(fp==NULL){
        perror("cannot open user profile");
        exit(1);
    }
    size =sizeof(user_t);
     
      printf("\nenter new email:");
	  fflush(stdin);
      scanf("%s",p1);
      printf("enter new password: ");
      fflush(stdin);
	  scanf("%s",p2);
	  fflush(stdin);
          strcpy(u->email,p1);
          strcpy(u->password,p2);
          printf("\n%s",u->password);
          fseek(fp,-size,SEEK_CUR);
          fwrite(u,size,1,fp);

          printf("\n password updated");

      

    fclose(fp);
}*/

void edit_profile(user_t *u)
{
	FILE *fp;
	char np[20],nm[20];
	long size = sizeof(user_t);
	fp = fopen(USER_DB, "rb+");
	if(fp == NULL) 
	{
		perror("cannot open user file");
		exit(1);
	}
	else
	{
		printf("email: ");
		scanf("%s", nm );
		printf("New password: ");
		scanf("%s", np);

		printf("%s %s\n",nm,np);

		strcpy(u->email,nm);
		strcpy(u->password,np);
		
		fseek(fp, -size, SEEK_CUR);
	
		fwrite(u, size, 1, fp);
		printf("\nNew password update: %s",u->password);
		fclose(fp);
	}
}

void user_change_password()
{
	int found = 0;
	char p[10];
	FILE *fp;
	user_t u;
	fp = fopen(USER_DB, "rb+");
	if(fp == NULL) 
	{
		perror("cannot open user file");
		exit(1);
	}
	while(fread(&u, sizeof(user_t), 1, fp) > 0) 
	{
			printf("Enter Old password: ");
			scanf("%s", p);
			if(strcmp(p,u.password) == 0 ) 
			{
				found = 1;
				break;
			}
			else
			{
				printf("Wrong Password\n");
			}
	}
	if(found) 
	{
		long size = sizeof(user_t);
		char np[10];
		printf("New password: ");
		scanf("%s", np);
		strcpy(u.password,np);
		fseek(fp, -size, SEEK_CUR);
		fwrite(&u, size, 1, fp);
		printf("Password updated.\n");
	}
	else 
		printf("User not found.\n");
	fclose(fp);
}

void user_display(user_t *u) {
	printf("%d, %s, %s, %s, %s\n", u->id, u->name, u->email, u->phone, u->role);
}

// book functions
void book_accept(book_t *b) 
{
	fflush(stdin);
	printf("Name: ");
	gets(b->name);
	fflush(stdin);
	printf("\nAuthor: ");
	gets(b->author);
	fflush(stdin);
	printf("\nSubject: ");
	gets(b->subject);
	fflush(stdin);
	printf("\nPrice: ");
	scanf("%lf", &b->price);
	fflush(stdin);
	printf("\nIsbn: ");
	gets(b->isbn);
	fflush(stdin);
}

void book_display(book_t *b) {
	printf("%d, %s, %s, %s, %.2lf, %s\n", b->id, b->name, b->author, b->subject, b->price, b->isbn);
}

// bookcopy functions
void bookcopy_accept(bookcopy_t *c) {
	// printf("id: ");
	// scanf("%d", &c->id);
	printf("book id: ");
	scanf("%d", &c->bookid);
	printf("rack: ");
	scanf("%d", &c->rack);
	strcpy(c->status, STATUS_AVAIL);
}

void bookcopy_display(bookcopy_t *c) {
	printf("Copy ID: %d,\n Book ID: %d,\n Rack Number:  %d,\n Status: %s\n", c->id, c->bookid, c->rack, c->status);
}

// issuerecord functions
void issuerecord_accept(issuerecord_t *r) 
{
	// printf("id: ");
	// scanf("%d", &r->id);
	printf("copy id: ");
	scanf("%d", &r->copyid);
	printf("member id: ");
	scanf("%d", &r->memberid);
	printf("issue date: ");
	date_accept(&r->issue_date);
	//r->issue_date = date_current();
	r->return_duedate = date_add(r->issue_date, BOOK_RETURN_DAYS);
	memset(&r->return_date, 0, sizeof(date_t));
	r->fine_amount = 0.0;
}

void issuerecord_display(issuerecord_t *r) {
	printf("issue record: %d, copy: %d, member: %d, fine: %.2lf\n", r->id, r->copyid, r->memberid, r->fine_amount);
	printf("issue ");
	date_print(&r->issue_date);
	printf("return due ");
	date_print(&r->return_duedate);
	printf("return ");
	date_print(&r->return_date);
}

void payment_accept(payment_t *p) 
{
	printf("member id: ");
	scanf("%d", &p->memberid);
	printf("type (fees/fine): ");
	scanf("%s", p->type);
	printf("amount: ");
	scanf("%lf", &p->amount);
	p->tx_time = date_current();
	if(strcmp(p->type, PAY_TYPE_FEES) == 0)
		p->next_pay_duedate = date_add(p->tx_time, MEMBERSHIP_MONTH_DAYS);
	else
		memset(&p->next_pay_duedate, 0, sizeof(date_t));
}

void payment_display(payment_t *p) 
{
	printf("payment: %d, member: %d, %s, amount: %.2lf\n", p->id, p->memberid, p->type, p->amount);
	printf("payment ");
	date_print(&p->tx_time);
	printf("payment due");
	date_print(&p->next_pay_duedate);
}

void book_find_by_name(char name[]) {
	FILE *fp;
	int found = 0;
	book_t b;
	// open the file for reading the data
	fp = fopen(BOOK_DB, "rb");
	if(fp == NULL) {
		perror("failed to open books file");
		return;
	}
	// read all books one by one
	while(fread(&b, sizeof(book_t), 1, fp) > 0) {
		// if book name is matching partially, found 1
		if(strstr(b.name, name) != NULL) {
			found = 1;
			book_display(&b);
		}
	}
	// close file
	fclose(fp);
	if(!found)
		printf("No such book found.\n");
}

int user_find_by_email(user_t *u, char email[]) {
	FILE *fp;
	int found = 0;
	// open the file for reading the data
	fp = fopen(USER_DB, "rb");
	if(fp == NULL) {
		perror("failed to open users file");
		return found;
	}
	// read all users one by one
	while(fread(u, sizeof(user_t), 1, fp) > 0) {
		// if user email is matching, found 1
		if(strcmp(u->email, email) == 0) {
			found = 1;
			break;
		}
	}
	// close file
	fclose(fp);
	// return
	return found;
}

int get_next_user_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(user_t);
	user_t u;
	// open the file
	fp = fopen(USER_DB, "rb");
	if(fp == NULL)
		return max + 1;
	// change file pos to the last record
	fseek(fp, -size, SEEK_END);
	// read the record from the file
	if(fread(&u, size, 1, fp) > 0)
		// if read is successful, get max (its) id
		max = u.id;
	// close the file
	fclose(fp);
	// return max + 1
	return max + 1;
}

int get_next_book_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(book_t);
	book_t u;
	// open the file
	fp = fopen(BOOK_DB, "rb");
	if(fp == NULL)
		return max + 1;
	// change file pos to the last record
	fseek(fp, -size, SEEK_END);
	// read the record from the file
	if(fread(&u, size, 1, fp) > 0)
		// if read is successful, get max (its) id
		max = u.id;
	// close the file
	fclose(fp);
	// return max + 1
	return max + 1;
}

int get_next_bookcopy_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(bookcopy_t);
	bookcopy_t u;
	// open the file
	fp = fopen(BOOKCOPY_DB, "rb");
	if(fp == NULL)
		return max + 1;
	// change file pos to the last record
	fseek(fp, -size, SEEK_END);
	// read the record from the file
	if(fread(&u, size, 1, fp) > 0)
		// if read is successful, get max (its) id
		max = u.id;
	// close the file
	fclose(fp);
	// return max + 1
	return max + 1;
}

int get_next_issuerecord_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(issuerecord_t);
	issuerecord_t u;
	// open the file
	fp = fopen(ISSUERECORD_DB, "rb");
	if(fp == NULL)
		return max + 1;
	// change file pos to the last record
	fseek(fp, -size, SEEK_END);
	// read the record from the file
	if(fread(&u, size, 1, fp) > 0)
		// if read is successful, get max (its) id
		max = u.id;
	// close the file
	fclose(fp);
	// return max + 1
	return max + 1;
}

int get_next_payment_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(payment_t);
	payment_t u;
	// open the file
	fp = fopen(PAYMENT_DB, "rb");
	if(fp == NULL)
		return max + 1;
	// change file pos to the last record
	fseek(fp, -size, SEEK_END);
	// read the record from the file
	if(fread(&u, size, 1, fp) > 0)
		// if read is successful, get max (its) id
		max = u.id;
	// close the file
	fclose(fp);
	// return max + 1
	return max + 1;
}



